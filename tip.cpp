#include <iostream>


int main(){
  float price;
  float tipperc;
  float totaltip;
  std::cout<< "Enter the price (pound): ";
  std::cin>> price;
  std::cout<< "Enter the tip percentage: ";
  std::cin>> tipperc;
  totaltip = price * (tipperc/100);
  std::cout<<"The tip is £"<< totaltip<<std::endl;
  std::cout<<"The total amount to pay is £"<<price+totaltip<<std::endl;
  

  return 0;
}
